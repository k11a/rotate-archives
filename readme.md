# Rotate-archives

Rotate-archives deletes files or folders to clear the set time periods between archives. For example, archives up to one week are stored daily, up to one month are stored weekly, up to one year are stored monthly, and so on.

The archive date is read from the time stamp of the file or directory name. For new archives a time stamp is added to the file or directory name. For example, the "archive.bak" is renamed to "2020-12-31_archive.bak". It works with files and directories the same way.

## Installation

1. place this script somewhere on your server, for example:  /usr/local/bin/rotate_archives.py
2. chmod a+x /usr/local/bin/rotate_archives.py
3. add a cron like this: * 1 * * * /usr/local/bin/rotate_archives.py > /dev/null

In step three, we added a cronjob for daily execution.

The script is used as a command line program:

    rotate-archives.py --help
    rotate-archives.py --version
    rotate-archives.py [test_mode=<off>] AgeFrom-Period-AmountLastTimeSlot=<7-5,31-14,365-180-5> archives_dir=</mnt/archives> [log_dir=</var/log/>] [log_size=<10>] [ftp_passive_mode=<on>] [debug_mode=<off>]

## Descritption parameters command line

`--help`

Show help.

`--version`

Show version.

`test_mode`

- Required: no
- Default value: enable
- Description: If enabled, does not delete archives.
- Values to enable: true, enable, on, yes, 1
- Values to disable: false, disable, off, no, 0

`AgeFrom-Period-AmountLastTimeSlot`

- Required: yes
- Description: sets the time slot through comma. Example for 3 time slot: 7-5,31-14,365-180-5. The first number of time slot sets the age of the archives that begin to belong to that time slot. Measured in days. The second number sets the time interval between archives to that time slot. Measured in days. The third number sets the maximum number of archives in the time slot. Set only for the last time slot.

For example, three slots 7-5,31-14,365-180-5:
- first time slot for archives age from 7 to 30 days with time interval between archives 5 days;
- second time slot for archives age from 31 to 364 days with time interval between archives 14 days;
- the third time slot for archives age from 365 to 365001 days with time interval between archives 180 days. 365001 days is set automatically.

`archives_dir`

- Required: yes
- Description: The folder in which archives will be deleted. For FTP: `ftp://<user>:<password>@<host>:<port>/<url-path>`

`log_dir`

- Required: no
- Default value: current directory
- Description: folder to place the log file. Log file name is 'rotate-archives.log'.

`log_size`

- Required: no
- Default value: 10
- Description: Log file size. Measured in MegaBytes. When this file is filled, it is save by appending the name extensions ‘.1’ and a new file with the initial name is silently opened for output.

`ftp_passive_mode`

- Required: no
- Default value: enable
- Description: Enable or not passive mode FTP protocol.
- Values to enable: true, enable, on, yes, 1
- Values to disable: false, disable, off, no, 0

`debug_mode`

- Required: no
- Default value: disable
- Description: More detailed log file.
- Values to enable: true, enable, on, yes, 1
- Values to disable: false, disable, off, no, 0

## Example
    rotate-archives.py test_mode=off AgeFrom-Period-AmountLastTimeSlot=7-5,31-14,365-180-5 archives_dir=/mnt/archives
